<?php
namespace model;

class DatabaseConnection {
    protected $bdd;

    public function __construct()
    {
        global $config;
        global $errors;
        try {
            $this->bdd = new \PDO("mysql:host=" . $config['db.host'] . ";dbname=" . $config['db.name'] . ";charset=utf8", $config['db.username'], $config['db.password']);
        } catch (PDOException $e) {
            $errors[] = $e->getMessage();
        }
    }

    public function close()
    {
        $this->bdd = null;
    }
}


