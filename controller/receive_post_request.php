<?php

use model\Measure;

// receive json data from post request
$data_json = file_get_contents('php://input');

// check json data
$data = json_decode($data_json) ;
if (! $data) {
    http_response_code(415);
    exit();
} elseif (! $data->temperature || ! $data->humidity) {
    http_response_code(400);
    exit();
}

// save measure
$measure = new Measure(date("Y-m-d H:i:s"), $data->temperature, $data->humidity);
$id = $measure->save();

if ($id > 0) {
    http_response_code(200);
} else {
    http_response_code(500);
    die();
}

